num = 600851475143
factor = 2
largest_factor = 1

def isPrime(number):
	for i in range (2, number):
		if number % i == 0:
			return False
	return True

while num > 1:
	if num % factor == 0:
		num /= factor
		if factor > largest_factor and isPrime(factor):
			largest_factor = factor
		factor = 2
	else:
		factor += 1

print largest_factor 

