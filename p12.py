def factors(n):
	facts = 0
	for i in range(1, int(n**0.5) + 1):
		if n % i == 0:
			facts += 2
	return facts

i = 1
triangle = 0
while True:
	triangle += i
	i += 1
	print str(triangle) + ":" + str(factors(triangle))
	if factors(triangle) > 500:
		print triangle
		break


