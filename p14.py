longest = 0

def collatz(i, j):
	if i == 1:
		return j
	if i % 2 == 0:
		return collatz(i/2, j+1)
	else:
		return collatz(3*i + 1, j+1)

for i in range (1, 1000000):
	curr = collatz(i, 0)
	if curr > longest:
		longest = curr
		print i


