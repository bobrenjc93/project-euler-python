sum = 0
last_last_fib = 1
last_fib = 1
while last_fib < 4000000:
	if last_fib % 2 == 0:
		sum += last_fib
	temp = last_last_fib
	last_last_fib = last_fib
	last_fib = last_fib + temp
print sum
