def isPalindrome(number):
	number_string = str(number)
	for i in range(0, len(number_string)/2):
		if number_string[i] != number_string[len(number_string) - 1 - i]:
			return False
	return True

largest_palindrome = 0
for i in range (0, 1000):
	for j in range (0, 1000):
		if i * j > largest_palindrome and isPalindrome(i * j):
			largest_palindrome = i * j

print largest_palindrome

