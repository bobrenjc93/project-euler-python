def prime(n):
	if n < 2:
		return False
	if n == 2:
		return True
	if n % 2 == 0:
		return False
	for i in range (3, int(n ** 0.5) + 1, 2):
		if n % i == 0:
			return False
	return True

total = 0
for i in range (2, 2000000):
	if prime(i):
		total += i

print total
