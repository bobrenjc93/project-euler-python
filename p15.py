xLimit = 22
yLimit = 22
done = {}
def totalMoves(x, y):
	total = 0
	if x == 21 and y == 21:
		return 1
	elif str(x) + " " + str(y) in done:
		return done[str(x) + " " + str(y)]

	if x + 1 < xLimit:
		total += totalMoves(x+1, y)
	if y + 1 < yLimit:
		total += totalMoves(x,y+1)
	done[str(x) + " " + str(y)] = total
	return total

print totalMoves(1, 1)

